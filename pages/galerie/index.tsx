// REACT BASIC
import React from 'react';
import GalerieComponent from '../../components/galerieComponent';
// Components
import HeaderComponent from '../../components/shared/headerComponent';
import TitleComponent from '../../components/shared/titleComponent';
// Lib
import {GetAllRealisations} from '../../lib/request';
import {Galerie} from '../../types/galerie';
// Types
import {RealisationEntity, RealisationRelationResponseCollection} from '../../types/realisations';
import {UploadFile} from '../../types/strapi';
// Utils
import {GetImg} from '../../lib/utils';

export default function Realisations(props: any) {
    const titleData: string = 'Rock’n Wood - Beaumont-Monteux | ARTISAN DE LA PIERRE ET FABRICANT DE CHARPENTE';
    const descriptionData: string = 'Découvrez notre galerie présentant nos réalisations variées. De la rénovation murale aux structures de charpente bois, explorez notre expertise en enduits imitation pierre, joints de pierre et charpenterie, illustrant notre savoir-faire et notre engagement pour des projets de construction et de rénovation de qualité';
    const title: string = 'Galerie photos';
    // Liste des réalisations
    const realisation: RealisationRelationResponseCollection = props.realisations;
    // Métadonnée card -> IMG
    let img: string = '';
    // Liste des images
    let images: Galerie[] = [];

    if (realisation && realisation.data && realisation.data[0] && realisation.data[0].attributes) {
        img = realisation.data[0].attributes?.after?.data?.attributes?.url.toString();

        realisation.data.forEach((real: RealisationEntity) => {
            let index = 0;
            const addImage = (data: UploadFile, r: RealisationEntity) => {
                if (data) {
                    if (images.find((img: Galerie) => img.src === GetImg(data.url))) return;
                    let title = r.attributes.title.toLowerCase().replace(/\s+/g, '_').replace(/[^a-z0-9_]+/gi, '');
                    index++;

                    images.push({
                        src: GetImg(data.url),
                        width: data.width,
                        height: data.height,
                        alt: `${title}_${index}`,
                        description: r.attributes.categorie.data.attributes.title,
                        link: `/realisations/${r.attributes.slug}`,
                    });
                }
            };

            addImage(real.attributes.after?.data?.attributes, real);
            if (real.attributes.before?.data?.attributes) addImage(real.attributes.before?.data.attributes, real);
            if (real.attributes.galerie && real.attributes.galerie?.data.length > 0) {
                real.attributes.galerie?.data.forEach((galerie: any) => {
                    addImage(galerie.attributes, real);
                });
            }
        });
    }

    return (
        <>
            <div className={'realisationsGallery columns is-multiline is-full container'}>
                {/*Métadonnée pour le référencement*/}
                <HeaderComponent title={titleData} description={descriptionData} img={GetImg(img)} icon={''}/>
                {/*Titre*/}
                <TitleComponent title={title}/>
                {/*Galerie*/}
                <GalerieComponent images={images}/>
            </div>
        </>
    );

}

export async function getStaticProps() {
    const {data} = await GetAllRealisations();
    const realisations: RealisationRelationResponseCollection = data.realisations;

    return {
        props: {realisations},
        revalidate: 60,
    };
}
