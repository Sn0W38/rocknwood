import React from 'react';
import { createStyles, Title, Text, Button, Container, Group } from '@mantine/core';
import Link from 'next/link';

const useStyles = createStyles((theme) => ({
  root: {
    paddingTop: 80,
    paddingBottom: 80,
  },

  label: {
    textAlign: 'center',
    fontWeight: 900,
    fontSize: 220,
    lineHeight: 1,
    marginBottom: theme.spacing.xl * 1.5,
    color: theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[2],

    [theme.fn.smallerThan('sm')]: {
      fontSize: 120,
    },
  },

  title: {
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    textAlign: 'center',
    fontWeight: 900,
    fontSize: 38,

    [theme.fn.smallerThan('sm')]: {
      fontSize: 32,
    },
  },

  description: {
    maxWidth: 500,
    margin: 'auto',
    marginTop: theme.spacing.xl,
    marginBottom: theme.spacing.xl * 1.5,
  },

  content: {
    display: 'flex',
    margin: 'auto',
    height: '100vh',

    div: {
      margin: 'auto'
    }
  }
}));

export default function FourOhFour() {
  const { classes } = useStyles();

  return (
    <div className={classes.content}>

      <Container className={classes.root}>
        <div className={classes.label}>404</div>
        <Title className={classes.title}>Vous avez trouvé un endroit secret.</Title>
        <Text color="dimmed" size="lg" align="center" className={classes.description}>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          Malheureusement, il s'agit uniquement d'une page 404. Vous avez peut-être fait une erreur dans l'adresse, ou la page a été
          été déplacée vers une autre URL.
        </Text>
        <Group position="center">
          <Button variant="subtle" size="md">
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <Link href={'/'} scroll={true}>Retourner à la page d'accueil</Link>
          </Button>
        </Group>
      </Container>

    </div>
  );
}
