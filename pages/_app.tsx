// CSS
import '../styles/globals.scss'
// REACT BASIC
import React from 'react';
import type { AppProps } from 'next/app'
// Lib
import { ApolloProvider } from '@apollo/client';
import { ParallaxProvider } from 'react-scroll-parallax';
//COMPONENTS
import NavBarComponent from '../components/shared/navBarComponent';
// LIB
import client from '../lib/apolloClient';
// Google Analytics
import { usePageViews } from 'nextjs-google-analytics';

export default function App({ Component, pageProps }: AppProps) {

  usePageViews();

  return (
    <>

      <ApolloProvider client={client}>
        {/*Init Graphql client*/}

        <ParallaxProvider>

          <NavBarComponent />
          {/*Bar de navigation*/}

          <Component {...pageProps} />
          {/*Métadonnée du site*/}

        </ParallaxProvider>
      </ApolloProvider>
    </>

  )
}
