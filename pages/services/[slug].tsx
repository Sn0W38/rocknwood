// React
import React from 'react';
import DefaultErrorPage from 'next/error'
// Lib
import { GetRealisationsByPage, GetService, GetServices } from '../../lib/request';
import { GetImg } from '../../lib/utils';
// Components
import TitleComponent from '../../components/shared/titleComponent';
import HeaderComponent from '../../components/shared/headerComponent';
import BlockRealisationComponent from '../../components/shared/blockRealisations';
// Types
import { Service, ServiceEntity, ServiceEntityResponse } from '../../types/services';
import { RealisationEntity } from '../../types/realisations';
import remarkGfm from "remark-gfm";
import Markdown from "react-markdown";

export default function Services(props: any) {

  const service: Service = props.service;
  const realisations: RealisationEntity[] = props.realisations;
  let img: string = '';

  // Ajoute la 1er image trouver pour les cards méta
  if (realisations && realisations.length !== 0 && realisations[0]) {
    img = GetImg(realisations[0].attributes.after.data.attributes.url);
  }

  if (service) {

    return (
      <>
        <HeaderComponent title={'Services: ' + service.title}
                         img={img}
                         description={service.description}
                         icon={service.icon?.data?.attributes?.url}
        />

        <div className={'serviceContent container'}>
          <div className={'columns is-multiline'}>
            {/*Titre*/}
            <div className={'column is-12'}>
              <TitleComponent title={service.title} />
            </div>
            {/*Description*/}
            <div className={'column is-8-desktop is-10-tablet is-10-mobile m-auto has-text-centered'}>
              <Markdown remarkPlugins={[remarkGfm]}>{service.description}</Markdown>
            </div>
            {/*Ligne de séparation*/}
            <div className={'column is-full'}>
              <hr/>

              <div className={'columns is-multiline carouselRealisations is-justify-content-center'}>
                <BlockRealisationComponent realisations={realisations} categorie={service.title} />
              </div>

            </div>
          </div>
        </div>

      </>
    )
  } else {
    return (
      // TODO 404
      <>
        <DefaultErrorPage  statusCode={404}/>
      </>
    )
  }

}

export async function getStaticPaths() {

  const { data } = await GetServices();
  const service: ServiceEntityResponse = data.services;

  const paths: ({params: {slug: string}})[] = service.data.map((service: ServiceEntity): {params: {slug: string}} => {
    return {
      params: {
        slug: `${service?.attributes?.slug}`
      }
    }
  });

  return {
    paths,
    fallback: true
  }
}


export async function getStaticProps(context: ({params: {slug: string}})) {
  // Slug de la page
  const id = context.params.slug;
  // Fetch les services pour avoir les bonnes data
  let { data } = await GetService(id);
  // Filtre par catégories
  const service: Service = data.services.data[0].attributes;
  // Récupère le titre de la catégorie
  const category = service.title.toString();
  // Récupère les réalisations filtrer par la catégorie
  data = await GetRealisationsByPage(1, category);
  const realisations: RealisationEntity[] = data.data.realisations;

  return {
    props: {
      service,
      realisations
    },
    revalidate: 60
  }
}
