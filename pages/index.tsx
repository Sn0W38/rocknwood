// REACT BASIC
import React  from 'react';
import { ApolloQueryResult } from '@apollo/client';
// LIB
import { GetInit } from '../lib/request';
// Components
import LoaderComponent from '../components/shared/loaderComponent';
import HeaderComponent from '../components/shared/headerComponent';
import HomeComponent from '../components/homeComponent';
import ServicesComponent from '../components/servicesComponent';
import PresentationComponent from '../components/presentationComponent';
import RealisationsComponent from '../components/realisationsComponent';
import FooterComponent from '../components/shared/footerComponent';
import RatingComponent from '../components/reviewsComponents';
import MapComponent from '../components/mapComponent';
import ContactComponent from '../components/contactComponent';
// Types
import { Service, ServiceEntity } from '../types/services';
import { Referencement, ReferencementEntity } from '../types/referencement';
import { PresentationPro } from '../types/presentationPro';
import { PresentationEntreprise } from '../types/presentationEntreprise';
import { RealisationEntity } from '../types/realisations';
import { CategorieEntity } from '../types/categories';
import { Mail, MailEntity } from '../types/mail';
import { AvisUtilisateurEntity } from '../types/review';

export default function Index(props:any) {

  // const projets = props.projets;
  const loading = props.loading;
  const seo: Referencement = props.seo.attributes;
  const pro: PresentationEntreprise = props.pro;
  const perso: PresentationPro = props.perso;
  const services: Service = props.services;
  const realisations: RealisationEntity[] = props.realisations;
  const mail: Mail = props.mail.attributes;
  const reviews: AvisUtilisateurEntity[] = props.reviews;

  if(loading) {
    return <LoaderComponent />
  } else {
    return (
      <>
        {/*Métadonnée du site (<Head></Head>)*/}
        <HeaderComponent title={seo.title}
                         titleUrl={seo.title_url}
                         img={seo?.background?.data.attributes.url}
                         description={seo.description}
                         icon={seo?.logo?.data.attributes.url}
        />

        <div className={'columns is-multiline'}>

          {/*1er vu du site*/}
          <HomeComponent background={seo?.background?.data.attributes}
                         title={seo.title}
                         subTitle={seo.sub_title}
          />

          <PresentationComponent pro={pro}
                                 perso={perso}
          />

          <svg className={'serviceBefore'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#F2F2F2" fillOpacity="1" d="M0,192L34.3,170.7C68.6,149,137,107,206,90.7C274.3,75,343,85,411,80C480,75,549,53,617,69.3C685.7,85,754,139,823,154.7C891.4,171,960,149,1029,128C1097.1,107,1166,85,1234,96C1302.9,107,1371,149,1406,170.7L1440,192L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"></path></svg>

          <ServicesComponent services={services}
                             id={services}
          />

          <svg className={'realisationBefore'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ded7cd" fillOpacity="1" d="M0,32L34.3,32C68.6,32,137,32,206,32C274.3,32,343,32,411,53.3C480,75,549,117,617,117.3C685.7,117,754,75,823,74.7C891.4,75,960,117,1029,112C1097.1,107,1166,53,1234,32C1302.9,11,1371,21,1406,26.7L1440,32L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z"></path></svg>

          <RealisationsComponent realisations={realisations}/>

          {/*S'il n'y a aucune évaluation*/}
          {reviews && reviews.length === 0 && (
            <svg  className={'reviewZero'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ded7cd" fillOpacity="1" d="M0,160L0,128L240,128L240,192L480,192L480,128L720,128L720,192L960,192L960,128L1200,128L1200,96L1440,96L1440,0L1200,0L1200,0L960,0L960,0L720,0L720,0L480,0L480,0L240,0L240,0L0,0L0,0Z"></path></svg>
          )}

          {/*S'il y a minimum 1 évaluation*/}
          {reviews && reviews.length !== 0 && (
            <svg className={'realisationAfter'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ded7cd" fillOpacity="1" d="M0,96L0,256L102.9,256L102.9,288L205.7,288L205.7,224L308.6,224L308.6,160L411.4,160L411.4,256L514.3,256L514.3,64L617.1,64L617.1,128L720,128L720,192L822.9,192L822.9,32L925.7,32L925.7,128L1028.6,128L1028.6,160L1131.4,160L1131.4,288L1234.3,288L1234.3,96L1337.1,96L1337.1,224L1440,224L1440,0L1337.1,0L1337.1,0L1234.3,0L1234.3,0L1131.4,0L1131.4,0L1028.6,0L1028.6,0L925.7,0L925.7,0L822.9,0L822.9,0L720,0L720,0L617.1,0L617.1,0L514.3,0L514.3,0L411.4,0L411.4,0L308.6,0L308.6,0L205.7,0L205.7,0L102.9,0L102.9,0L0,0L0,0Z"></path></svg>
          )}

          {reviews && reviews.length !== 0 && (
            <RatingComponent reviews={reviews} />
          )}

          {reviews && reviews.length !== 0 && (
            <svg className={'contactAfter'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#F2F2F2" fillOpacity="1" d="M0,96L17.1,101.3C34.3,107,69,117,103,149.3C137.1,181,171,235,206,245.3C240,256,274,224,309,192C342.9,160,377,128,411,106.7C445.7,85,480,75,514,101.3C548.6,128,583,192,617,186.7C651.4,181,686,107,720,90.7C754.3,75,789,117,823,160C857.1,203,891,245,926,234.7C960,224,994,160,1029,154.7C1062.9,149,1097,203,1131,202.7C1165.7,203,1200,149,1234,117.3C1268.6,85,1303,75,1337,69.3C1371.4,64,1406,64,1423,64L1440,64L1440,320L1422.9,320C1405.7,320,1371,320,1337,320C1302.9,320,1269,320,1234,320C1200,320,1166,320,1131,320C1097.1,320,1063,320,1029,320C994.3,320,960,320,926,320C891.4,320,857,320,823,320C788.6,320,754,320,720,320C685.7,320,651,320,617,320C582.9,320,549,320,514,320C480,320,446,320,411,320C377.1,320,343,320,309,320C274.3,320,240,320,206,320C171.4,320,137,320,103,320C68.6,320,34,320,17,320L0,320Z"></path></svg>
          )}

          {/*Terminado*/}

          <ContactComponent services={services} mail={mail} />

          <MapComponent />

          <FooterComponent title={seo.title}
                           adresse={seo.adresse}
                           phone={seo.phone}
                           email={seo.email}
                           facebook={seo.facebook}
                           instagram={seo.instagram}
                           services={services}
                           reviews={reviews}
          />

        </div>

      </>
    )
  }
}


export async function getStaticProps() {
  const data: ApolloQueryResult<any> = await GetInit();

  // ===============================================
  // DATAS
  const services: ServiceEntity[] = data.data.services.data;
  const seo: ReferencementEntity = data.data.referencement.data;
  const perso: PresentationPro = data.data.presentationPro.data.attributes;
  const pro: PresentationEntreprise = data.data.presentationEntreprise.data.attributes;
  const realisations: RealisationEntity[] = data.data.realisations.data;
  const categories: CategorieEntity[] =  data.data.categories.data;
  const mail: MailEntity =  data.data.mail.data;
  const reviews: AvisUtilisateurEntity[] = data.data.avisUtilisateurs.data;
  // ===============================================

  return {
    props: {
      seo,
      perso,
      pro,
      services,
      realisations,
      categories,
      mail,
      reviews,
      loading: data.loading,
      networkStatus: data.networkStatus
    },
    revalidate: 60
  }
}
