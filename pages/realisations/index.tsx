// REACT BASIC
import React from 'react';
// Components
import HeaderComponent from '../../components/shared/headerComponent';
import TitleComponent from '../../components/shared/titleComponent';
import BlockRealisationComponent from '../../components/shared/blockRealisations';
// Lib
import {  GetRealisationsByPage } from '../../lib/request';
// Types
import { RealisationRelationResponseCollection } from '../../types/realisations';
import { GetImg } from '../../lib/utils';

export default function Realisations(props:any) {
  const titleData = 'Rock’n Wood - Beaumont-Monteux | ARTISAN DE LA PIERRE ET FABRICANT DE CHARPENTE';
  const descriptionData = 'Découvrez nos réalisations; témoignage de notre habileté et d’un savoir-faire artisanal maîtrisé. Notre équipe pluridisciplinaire reste à votre entière disposition pour mener à bien vos projets de rénovations et d’embellissements à venir.';

  const title = 'Nos réalisations';
  // Liste des réalisations
  const realisation: RealisationRelationResponseCollection = props.realisations;
  // Métadonnée card -> IMG
  let img = '';
  // Vérifie qu'une image est présente

  if (realisation && realisation.data && realisation.data[0] && realisation.data[0].attributes) {
    img = realisation.data[0].attributes?.after?.data?.attributes?.url.toString();
  }

  return (
    <>
      <div className={'allRealisationsBody columns is-multiline is-full container'}>
        {/*Métadonnée pour le référencement*/}
        <HeaderComponent  title={titleData}
                          description={descriptionData}
                          img={GetImg(img)}
                          icon={''}
        />

        {/*Titre de la réalisation*/}
        <TitleComponent title={title} />
        <div className={'column has-text-centered'}>
          <p>Plongez dans l’univers de nos réalisations.<br/>Découvrez une diversité de projets, chacun incarnant notre
            engagement envers l’artisanat traditionnel, la créativité et la qualité, le tout au service de vos
            exigences.
          </p>
        </div>
        <div>
          <BlockRealisationComponent realisations={realisation} categorie={'*'} />
        </div>

      </div>
    </>
  )

}

export async function getStaticProps() {
  const { data  } = await GetRealisationsByPage(1);
  const realisations: RealisationRelationResponseCollection = data.realisations;

  return {
    props: {
      realisations
    },
    revalidate: 60
  }
}
