// React
import React from 'react';
import DefaultErrorPage from 'next/error';
// React Image comparaison
import ReactCompareImage from 'react-compare-image';
// Gallery
import GalerieComponent from '../../components/galerieComponent';

// Lib
import {GetImg} from '../../lib/utils';
import {GetRealisations, GetRealisationsBySlug} from '../../lib/request';
// Component
import HeaderComponent from '../../components/shared/headerComponent';
import TitleComponent from '../../components/shared/titleComponent';
// Types
import {Realisation, RealisationEntity, RealisationRelationResponseCollection} from '../../types/realisations';
import Moment from 'react-moment';
import Link from 'next/link';
import Image from 'next/image';
import {UploadFileEntity} from '../../types/strapi';

export default function Realisations(props: { realisations: { data: [RealisationEntity] } }) {
    const realisation: Realisation = props?.realisations?.data[0]?.attributes;

    if (realisation && realisation.after && realisation.title) {
        let beforeImg = '';
        let afterImg = realisation.after.data.attributes.url.toString();
        let images: any[] = [];


        if (realisation.before && realisation.before.data && realisation.before.data.attributes && realisation.before.data.attributes.url) {
            beforeImg = realisation.before.data.attributes.url.toString();
        }


        if (realisation.galerie && realisation.galerie.data.length > 0) {

            images = realisation.galerie.data.map((image: UploadFileEntity, index: number) => {
                let title = realisation.title.toLowerCase().replace(/\s+/g, '_').replace(/[^a-z0-9_]+/gi, '');

                return {
                    src: GetImg(image.attributes.url),
                    alt: `${title}_${index}`,
                    width: image.attributes.width,
                    height: image.attributes.height
                };
            });

            console.log(images);

        }

        const title = realisation.title.toString();
        const categorie = realisation.categorie?.data.attributes.title;

        // @ts-ignore
        return (
            <>
                <div className={'realisationBody columns is-multiline is-full container'}>
                    {/*Métadonnée pour le référencement*/}
                    <HeaderComponent title={realisation.seotitle}
                                     description={realisation.seodescription}
                                     keywords={realisation.seokeywords}
                                     img={realisation.after.data.attributes.url}
                                     icon={''}
                    />


                    <div className={'realisationContent column is-full columns is-multiline container'}>

                        <div className={'is-full-desktop columns'}>

                            <div className={'realisationInformation column is-5-desktop'}>
                                <div>
                                    <TitleComponent title={realisation.title}/>

                                    <div className={'columns is-multiline column is-full-desktop'}>
                                        <h2 className={'column is-6 p-0 has-text-left categories'}>{categorie}</h2>
                                        <div className={'column p-0 has-text-right'}><Moment date={realisation.publishedAt} format={'DD/MM/YYYY'}/></div>
                                    </div>
                                </div>

                                <div>
                                    <p className={'content'}>{realisation.content}</p>
                                </div>
                            </div>

                            <div className={'column is-7-desktop m-auto'}>

                                {/*Une seule image de disponible*/}
                                {!beforeImg && (
                                    <div className={'imageRealisation'}>
                                        <Image src={afterImg} layout={'responsive'} width={100} height={100}
                                               alt={title}/>
                                    </div>
                                )}
                                {/*Les 2 images sont disponibles*/}
                                {beforeImg && (
                                    <div className={'imageRealisation'}>
                                        <ReactCompareImage leftImage={`${GetImg(beforeImg)}`}
                                                           rightImage={`${GetImg(afterImg)}`}
                                                           leftImageLabel="Avant"
                                                           rightImageLabel="Après"
                                        />
                                    </div>
                                )}
                            </div>

                        </div>

                        {images && images.length > 0 && (

                            <div className={'is-full-desktop columns gallery'}>
                                <h2>Galerie photo du projet</h2>
                                {/*Galerie d'images*/}
                                <GalerieComponent images={images}/>
                            </div>
                        )}


                        <div className={'otherRealisation column is-full is-flex mb-6'}>
                            <Link href={'/realisations'} scroll={true}>
                                <button className={'button is-outlined is-info'}>
                                    Voir plus
                                </button>
                            </Link>
                        </div>

                    </div>

                </div>


            </>
        );
    } else {
        return (
            // TODO 404
            <>
                <DefaultErrorPage statusCode={404}/>
            </>
        );
    }

}

export async function getStaticPaths() {

    const {data} = await GetRealisations();
    const realisations: RealisationRelationResponseCollection = data.realisations;
    const paths = realisations.data.map((prj: RealisationEntity) => {

        return {
            params: {
                slug: `${prj.attributes.slug}`,
            },
        };
    });

    return {
        paths,
        fallback: true,
    };
}

export async function getStaticProps(context: any) {
    const id: String = context.params.slug;
    const {data} = await GetRealisationsBySlug(id);
    const realisations: RealisationEntity = data.realisations;

    return {
        props: {
            realisations,
        },
        revalidate: 60,
    };
}