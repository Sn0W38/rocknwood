import React from 'react';

export default function Legal() {

  return (
    <div className={'legalBody container sections'}>
      <div className={'columns is-multiline'}>

      <section className={'column is-full'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h1 className={'title'}>Mentions légales et politique de confidentialité</h1>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>1 - Édition du site</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site internet <a href='https://rockn-wood.fr'>rock'n Wood</a> l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi:</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle mt-2 mb-0'}>Propriétaire du site :</h3>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Gérant: AZEMARD Florian</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Contact : azemard.florian@rockn-wood.fr, 0782361637</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Adresse : 330 route de la veaune 26600 Beaumont-Monteux .</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle mt-2 mb-0'}>Identification de l'entreprise: EURL Gérant: AZEMARD Florian</h3>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Société EURL: Rock'n Wood au capital social de 5000€ - SIREN : 919846378 - RCS ou RM : 919846378 - Adresse postale : 330 route de la veaune 26600 Beaumont-Monteux</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle mt-2 mb-0'}>Directeur de la publication :</h3>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>AZEMARD Florian Contact : azemard.florian@rockn-wood.fr</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle mt-2 mb-0'}>Hébergeur :</h3>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Amazon EU SARL 5 Rue Plaetis L- 2338 Luxembourg Grand Duché du Luxembourg +3525226733300</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle mt-2 mb-0'}>Délégué à la protection des données :</h3>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>azemard florian - azemard.florian@rockn-wood.fr</p>

        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle mt-2 mb-0'}>Autres contributeurs: </h2>
        <ul>
          <li><a href="https://www.flaticon.com/free-icons/construction" title="construction icons">Construction icons created by Freepik - Flaticon</a></li>
          <li><a href="https://www.flaticon.com/free-icons/saw" title="saw icons">Saw icons created by berkahicon - Flaticon</a></li>
          <li><a href="https://www.flaticon.com/free-icons/construction" title="construction icons">Construction icons created by wanicon - Flaticon</a></li>
        </ul>
        </section>

      <section className={'column is-full'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>2 - Propriété intellectuelle et contrefaçons.</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p  className={'has-text-justified'}>Gérant: AZEMARD Florian Société EURL: Rock'n Wood est propriétaire des droits de propriété intellectuelle et détient les droits d’usage sur tous les éléments accessibles sur le site internet, notamment les textes, images, graphismes, logos, vidéos, architecture, icônes et sons.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable du Gérant AZEMARD Florian.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p>
      </section>


      <section className={'column is-full'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>3 - Limitations de responsabilité.</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Gérant AZEMARD Florian ne pourra être tenu pour responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site <a href='https://rockn-wood.fr'>rock'n Wood</a>.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Gérant AZEMARD Florian Société EURL: Rock'n Wood décline toute responsabilité quant à l’utilisation qui pourrait être faite des informations et contenus présents sur <a href='https://rockn-wood.fr'>rock'n Wood</a>.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Société EURL: Rock'n Wood s’engage à sécuriser au mieux le site <a href='https://rockn-wood.fr'>rock'n Wood</a>, cependant sa responsabilité ne pourra être mise en cause si des données indésirables sont importées et installées sur son site à son insu.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Des espaces interactifs (espace formulaire de contact ou commentaires) sont à la disposition des utilisateurs. Gérant: AZEMARD Florian Société EURL: Rock'n Wood se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Le cas échéant, Gérant: AZEMARD Florian se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie ...).</p>
      </section>

      <section className={'column is-full'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>4 - CNIL et gestion des données personnelles.</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Conformément aux dispositions de la loi 78-17 du 6 janvier 1978 modifiée, l’utilisateur du site <a href='https://rockn-wood.fr'>rock'n Wood</a> dispose d’un droit d’accès, de modification et de suppression des informations collectées. Pour exercer ce droit, envoyez un message à notre Délégué à la Protection des Données : AZEMARD Florian - azemard.florian@rockn-wood.fr.</p>
      </section>

      <section className={'column is-full'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>5 - Liens hypertextes et cookies</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Le site <a href='https://rockn-wood.fr'>rock'n Wood</a> contient des liens hypertextes vers d’autres sites et dégage toute responsabilité à propos de ces liens externes ou des liens créés par d’autres sites vers <a href='https://rockn-wood.fr'>rock'n Wood</a>.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>La navigation sur le site <a href='https://rockn-wood.fr'>rock'n Wood</a> est susceptible de provoquer l’installation de cookie(s) sur l’ordinateur de l’utilisateur.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Un "cookie" est un fichier de petite taille qui enregistre des informations relatives à la navigation d’un utilisateur sur un site. Les données ainsi obtenues permettent d'obtenir des mesures de fréquentation, par exemple.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Vous avez la possibilité <b>d'accepter</b> ou de <b>refuser</b> les cookies en modifiant les paramètres de votre navigateur. Aucun cookie ne sera déposé sans votre consentement.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified mb-4'}>Les cookies sont enregistrés pour une durée maximale de 6 mois.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>Pourquoi avons-nous besoin d'utiliser des cookies ?</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified mb-4'}>Sans cookies, vous seriez traité comme un nouveau visiteur à chacune de vos visites sur une page web par le serveur sur lequel cette page se trouve. Cela serait naturellement très agaçant pour vous, mais cela rendrait également nos sites complètement inutilisables. Un cookie se comporte comme une clé : cela nous permet de vous offrir une expérience plus rapide, de se souvenir des réglages que vous avez apportés dans votre profil et de ne pas afficher les mêmes messages en boucle.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>Nous utilisons également les solutions de services tiers: Cookies analytiques</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified mb-4'}>Il s'agit de cookies qui sont utilisés pour stocker des informations sur la façon dont vous utilisez nos services et visitez notre plateforme (par exemple, le temps passé sur notre plateforme, les pages visitées, les liens cliqués, le site qui vous a envoyé vers notre page, etc.) ainsi que pour mesurer la performance de nos campagnes marketing.</p>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h3 className={'subtitle'}>Google Ireland Limited - Gordon House, Barrow Street, Dublin 4, IRLANDE.</h3>
        <ul className={'bg_legal mb-2'}>
          <li>ga • 24 mois</li>
          <li>_gali • 30 secondes</li>
          <li>_gat • 1 minute</li>
          <li>_gid • 24 heures</li>
        </ul>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Nous utilisons Google Analytics afin de collecter des informations relatives au nombre de visiteurs qui naviguent sur notre site, et sur la manière dont ils l'utilisent. Nous utilisons ces informations afin de compiler des rapports et nous aider à améliorer notre site. En savoir plus sur leurs <a href={'https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage'}>cookies</a> et leur <a href={'https://policies.google.com/privacy'}>Politique de confidentialité</a>.</p>
      </section>

      <section className={'column is-full mb-6'}>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <h2 className={'subtitle'}>6 - Droit applicable et attribution de juridiction.</h2>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <p className={'has-text-justified'}>Tout litige en relation avec l’utilisation du site <a href='https://rockn-wood.fr'>rock'n Wood</a> est soumis au droit français. En dehors des cas où la loi ne le permet pas, il est fait attribution exclusive de juridiction aux tribunaux compétents de Valence.</p>
      </section>

    </div>

  </div>
  );
}
