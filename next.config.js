/** @type {import('next').NextConfig} */
var path = require("path");

const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  },
  images: {
    loader: 'imgix',
    path: process.env.UPLOADS,
    disableStaticImages: false,
    domains: ['localhost', 'api.rockn-wood.fr'],
    minimumCacheTTL: 60
  },
  env: {
    API: process.env.API,
    TOKEN: process.env.TOKEN,
    UPLOADS: process.env.UPLOADS,
    FRONT: process.env.FRONT,
    EMAIL: process.env.EMAIL,
    PASSWORD: process.env.PASSWORD,
    RECAPTCHA: process.env.RECAPTCHA
  },
  compiler: {
    // removeConsole: true
  }
}

module.exports = nextConfig
