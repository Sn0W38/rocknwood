This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Lib
Image slider: 
* https://www.npmjs.com/package/react-compare-slider
* https://react-compare-slider.vercel.app/?path=/story/demos-handles--completely-custom
* https://reactjsexample.com/simple-react-component-to-compare-two-images-using-slider/

Gallery:
* https://yet-another-react-lightbox.com/examples/nextjs
* https://www.npmjs.com/package/react-photo-album

Google Analytics
* https://github.com/MauricioRobayo/nextjs-google-analytics

## NodeJs Version
* nvm install 16.15.0
* nvm use 16.15.0

## Install node_modules
* Yarn install
* npm i --force

## Icones
* <a href="https://www.flaticon.com/fr/icones-gratuites/spatule" title="spatule icônes">Spatule icônes créées par Freepik - Flaticon</a>
* <a href="https://www.flaticon.com/fr/icones-gratuites/brique" title="brique icônes">Brique icônes créées par Eucalyp - Flaticon</a>
* <a href="https://www.flaticon.com/fr/icones-gratuites/toit" title="toit icônes">Toit icônes créées par Freepik - Flaticon</a>
