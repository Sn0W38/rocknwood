// React
import React from 'react';
import Link from 'next/link';
// Lib
import { Fade } from 'react-awesome-reveal';
import Image from 'next/image';


export default function CardServicesComponent(props: any) {

  const title = props.title;
  const description_short = props.description_short;
  const icon = props.icon;
  const slug = props.slug;

  return (

    <>
      <div className={'cardService'}>
        {/*@ts-ignore*/}
        <Fade direction={'up'}>
          <Link href={`/services/${slug}`}>
            <div className={'columns is-multiline'}>

              <div className={'contentHeader column is-full'}>
                <div className={`iconService ${slug}`}>
                  <figure className="image is-48x48">
                    <Image src={icon} layout={'responsive'} width={100} height={100} alt={title} />
                  </figure>
                </div>
              </div>

              <div className={`content column is-full ${slug}`}>
                <h3>{title}</h3>

                <p>{description_short}</p>

                <a>Voir plus...</a>
              </div>

            </div>
          </Link>
        </Fade>
      </div>

    </>

  )

}
