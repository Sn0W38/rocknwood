// React
import React from 'react';
// Mantis
import { createStyles, Card, Group, Center, Button, Text, } from '@mantine/core';
// Lib
import { Fade } from 'react-awesome-reveal';
import Moment from 'react-moment';
import { GetImg } from '../../lib/utils';
// Types
import { RealisationEntity } from '../../types/realisations';

export default function CardRealisationComponent(props: {realisations: RealisationEntity, isHome:boolean }) {

  const isHome = props.isHome;
  const id = props.realisations?.id;
  const title = props.realisations?.attributes.title.toString();
  const slug = props.realisations?.attributes.slug.toString();
  const after = props.realisations?.attributes.after?.data.attributes.url.toString();
  const before = props.realisations?.attributes?.before?.data?.attributes?.url?.toString()  || '';
  const content = props.realisations?.attributes.content.toString();
  const categories = props.realisations?.attributes.categorie?.data?.attributes?.title?.toString();
  const created = props.realisations?.attributes.createdAt;
  // Mantis Styles
  const useStyles = createStyles((theme, _params, getRef) => {
    const image = getRef('image');

    return {
      card: {
        position: 'relative',
        height: 300,
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],

        [`&:hover .${image}`]: {
          transform: 'scale(1.03)',
        },
      },

      image: {
        ref: image,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundSize: 'cover',
        transition: 'transform 500ms ease',
      },

      overlay: {
        position: 'absolute',
        top: '20%',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundImage: 'linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, .85) 90%)',
      },

      content: {
        height: '100%',
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        zIndex: 1,
      },

      title: {
        color: theme.white,
        marginBottom: 5,
      },

      bodyText: {
        color: 'white',
        textAlign: 'end'
      },

      author: {
        color: theme.colors.dark[2],
      },
    };
  });

  const { classes, theme } = useStyles();

  return (
    <>
      {/*@ts-ignore*/}
      <Fade>
        <section className={'cardRealisation'}>

          <Card
            p="lg"
            shadow="lg"
            className={classes.card}
            radius="md"
            // component="a"
            // href={`/realisations/${slug}`}
            // target="_blank"
          >
            <div className={classes.image} style={{ backgroundImage: `url(${GetImg(after)})` }}>

            </div>
            <div className={classes.overlay} />
            <div className={classes.content}>
              <div>

                <Text size="lg" className={classes.title} weight={500}>
                  {isHome ? (<h3>{title}</h3>) : (<h2>{title}</h2>)}
                </Text>

                <Group position="apart" spacing="xs">
                  <Text size="sm" className={classes.author}>
                    {categories} - <Moment date={created} format={'DD/MM/YYYY'} />
                  </Text>

                  <Group spacing="lg">
                    <Center>
                      <Button variant="white"
                              color="dark"
                              component="a"
                              size="xs"
                              href={`/realisations/${slug}`}
                              target="_blank"
                      >
                        Voir plus
                      </Button>
                    </Center>
                  </Group>
                </Group>
              </div>
            </div>
          </Card>
        </section>

      </Fade>


    </>

  )

}
