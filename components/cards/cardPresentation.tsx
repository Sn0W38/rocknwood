// React
import React from 'react';
import { Parallax } from 'react-scroll-parallax';
// Lib
import { GetImg } from '../../lib/utils';
// Types
import { PresentationPro } from '../../types/presentationPro';
import { PresentationEntreprise } from '../../types/presentationEntreprise';
import Image from 'next/image';
import Markdown from "react-markdown";
import remarkGfm from "remark-gfm";

export default function CardPresentationComponent(props: any) {
  const information: PresentationPro | PresentationEntreprise = props.information;
  const pos: string = props.position.toString();
  let style = {};
  let imgSrc: string = '';

  if (pos && information && information.img) {
    imgSrc = information.img.data.attributes.url.toString();
    style = {
      backgroundImage: `url(${GetImg(imgSrc)})`,
    }
  }

  return (
    <>
      <div className={'column is-full container columns is-multiline sectionPres m-auto'}>

        {pos === 'right' && (
          <div className={'column is-7-desktop is-7-tablet is-12-mobile contentImg'}>
            <figure className={'image is-fullwidth'}>
              <Image src={imgSrc}
                     layout={'responsive'}
                     width={100}
                     height={100}
                     alt={information.title.toString()} />
            </figure>
          </div>
        )}

        <Parallax className={'column is-5-desktop is-5-tablet is-12-mobile pres'} speed={200} translateY={[30, -30]}>
          <div className={`contentPresentation ${pos}`}>
            <h3>{information.title}</h3>
            <Markdown remarkPlugins={[remarkGfm]}>{information.content}</Markdown>
          </div>
        </Parallax>

        {pos === 'left' && (
          <div className={'column is-7 is-7-tablet is-12-mobile contentImg'}>
            <figure className={'image is-fullwidth'}>
              <Image src={imgSrc}
                     layout={'responsive'}
                     width={100}
                     height={100}
                     alt={information.title.toString()} />
            </figure>
          </div>
        )}


      </div>

    </>

  )

}
