// React
import React from 'react';
import { FaUserAlt } from "react-icons/fa";
// @ts-ignore
import ReactStars from 'react-rating-stars-component';

export default function CardReview(props: any) {

  const name = props.name;
  const review = props.review;
  const score = props.score;

  return (
    <>
      <div className={'columns is-multiline m-4 ratingReview'}>

        <div className={'column is-12 stars'}>
          <div className={'starsContent'}>
            <ReactStars
              edit={false}
              count={5}
              value={score}
              size={25}
              activeColor="#ffd700"
            />
          </div>
        </div>

        <div className={'contentReview column is-full'}>
          <div>
            <p>{review}</p>
          </div>
        </div>

        <div className={'authorSection column is-full columns is-multiline'}>
          <div className={'column is-2-desktop is-2-tablet is-2-mobile iconReview'}>
            <div className={'circle'}>
              <div>
                <FaUserAlt />
              </div>
            </div>
          </div>

          <div className={'column is-10-desktop is-10-tablet is-10-mobile author'}>
            <b>{name}</b>
          </div>
        </div>

      </div>
    </>

  )

}
