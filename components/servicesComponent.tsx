// react
import React from 'react';
// Components
import TitleComponent from './shared/titleComponent';
import CardServicesComponent from './cards/cardServices';
// Types
import { ServiceEntity } from '../types/services';

export default function ServicesComponent(props: any) {
  let listService: any[] = [];

  const services: ServiceEntity[] = props.services;

  if (services && services.length !== 0) {
    const clas = 'column is-10-mobile is-5-tablet is-3-desktop';

    listService = services.map((srv: ServiceEntity, index: number) => {
      if (srv && srv.attributes && srv.attributes.title && srv.id) {
        const id = srv.id || index;

        return <div className={clas} key={id.toString()}>
          <CardServicesComponent title={srv.attributes.title}
                                 description_short={srv.attributes.description_short}
                                 description={srv.attributes.description}
                                 icon={srv.attributes?.icon?.data?.attributes?.url}
                                 slug={srv.attributes.slug}
          />
        </div>

      }
    });
  }


  return (
    <>
      <div id={'services'} className={'column is-full bodyServices spaceServices'}>
        <div className={'container column is-full sections'}>
          <TitleComponent title={'Nos services'} isHome={true}/>

          <div className={'column has-text-centered bodyServicesText'}>
            <p>Découvrez notre large éventail de services de rénovation allant de la rénovation murale aux <a title={""} href={"https://rockn-wood.fr/services/enduit-aspect-pierre"}> enduits imitation pierre</a>, en passant par la précision des <a title="" href={"https://rockn-wood.fr/services/joints-de-pierre"}>joints de pierre</a> et pour finir, par notre expertise en <a title="" href={"https://rockn-wood.fr/services/charpente-bois"}>charpente bois</a>.</p>
            <p>{'Notre approche met en avant la relation client, la proximité et une qualité d\'exécution guidée par l\'engagement humain, pour des projets sur mesure et une satisfaction totale.'}</p>
          </div>

          <div className={'columns is-multiline cardSection'}>
            {listService}
          </div>
        </div>
      </div>
    </>

  )

}
