// React
import { useState } from 'react';
// Map Leaflet
import 'leaflet/dist/leaflet.css';
import { MapContainer, TileLayer, Marker } from 'react-leaflet';
import { icon } from "leaflet"
// Lib
import { GetImg } from '../lib/utils';


export default function Map() {
  const [geoData, setGeoData] = useState({ lat: 45.0414217, lng: 4.9334938});

  // Icon de strappi
  const ICON = icon({
    iconUrl: GetImg('/uploads/1200px_Google_Maps_pin_svg_b060080d64.png?updated_at=2022-07-01T07:08:27.052Z'),
    iconSize: [25, 45],
  })
  // Romans sur Isère
  const center: any = [geoData.lat, geoData.lng];

  return (
    <MapContainer center={center}
                  zoom={12}
                  style={{ height: '60vh' }}
                  scrollWheelZoom={false}
    >
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      {geoData.lat && geoData.lng && (
        <Marker position={[geoData.lat, geoData.lng]} icon={ICON} />
      )}
    </MapContainer>
  );
}
