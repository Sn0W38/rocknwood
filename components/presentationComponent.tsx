// React
import React from 'react';
// Components
import VeticalLineComponent from './shared/verticalLineComponent';
import CardPresentationComponent from './cards/cardPresentation';
import TitleComponent from './shared/titleComponent';
// Lib
import { Fade } from "react-awesome-reveal";
// Types
import { PresentationEntreprise } from '../types/presentationEntreprise';
import { PresentationPro } from '../types/presentationPro';

export default function PresentationComponent(props: any) {
  const pro: PresentationEntreprise = props.pro;
  const perso: PresentationPro = props.perso;

  return (

    <>
      <div id={'pres'} className={'column is-full sections'}>

        {/*<VeticalLineComponent trigger={'presentationComponentTop0'} />*/}

        <TitleComponent title={'A propos'} isHome={true}/>

        <VeticalLineComponent trigger={'presentationComponentTop1'} />

        <div className={'column is-full bodyPresentation sectionSpace container'}>
          {/*@ts-ignore*/}
          <Fade>
            <CardPresentationComponent information={pro} position={'right'} />
          </Fade>
        </div>

        <VeticalLineComponent trigger={'presentationComponentMid'} />

        <div className={'column is-full bodyPresentation sectionSpace container'}>
          {/*@ts-ignore*/}
          <Fade>
            <CardPresentationComponent information={perso} position={'left'} />
          </Fade>
        </div>

      </div>


    </>

  )

}
