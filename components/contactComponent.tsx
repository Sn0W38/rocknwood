// React
import React, { useRef, useState } from 'react';
// React Form
import { useForm } from 'react-hook-form';
// Components
import TitleComponent from './shared/titleComponent';
// Lib
import { TextInput, Button, Textarea, Checkbox, CheckboxGroup } from '@mantine/core';
import { captcha, sendEmail } from '../lib/request';
// Types
import { ServiceEntity } from '../types/services';
import { formEmailData } from '../types/form';
import { Mail } from '../types/mail';
// Toast
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// Fin Toast
// Recaptcha
import ReCAPTCHA from 'react-google-recaptcha';
// Fin Recaptcha

export default function ContactComponent(props: any) {
  // ================================
  // Key pour reCaptcha GOOGLE
  const recaptchaPublicKey: string = process.env.RECAPTCHA || '';
  // Recaptcha google
  const captchaRef: any = useRef(null);
  // ================================
  // Liste des services disponibles
  const services: ServiceEntity[] = props.services;
  // Information du contact mail
  const mail: Mail = props.mail;
  // Config Form Control
  const { reset, register, setValue, formState: { errors }, trigger, handleSubmit, clearErrors } = useForm<formEmailData>({
    mode: "onChange",
    shouldUseNativeValidation: false,
    shouldFocusError: false,
    shouldUnregister: false,
    delayError: undefined
  });

  // Changement de statut du google reCaptcha
  function handleChange(value: any) {
    if (value) {
      captcha(value).then((res: boolean) => {
        // console.log('gp isValid -> ', res);
        setIsValid(res);
      })
    }
  }

  // Liste des services (Checkbox group)
  const checkboxService = services.map((serv: ServiceEntity, index: any) => {
    if (serv && serv.id && serv.attributes.title) {
      const id = serv.id.toString() || index;
      return <Checkbox key={id}
                       value={serv.attributes.title.toString()}
                       label={serv.attributes.title}
        />
    }
  })

  const [value, setValueCheck] = useState<string[]>([]);
  const [isValid, setIsValid] = useState<boolean>(false);

  // Valide le formulaire
  const onSubmit = (data: formEmailData) => {
    if (isValid) {
      sendEmail(data).then((res) => {
        if (res.status === 200) {
          toast.success('Votre message a bien été envoyé, Merci.', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });

          // Reset les champs du formulaire
          reset();
          setValueCheck([]);

        } else {
          toast.error('Une erreur s\'est produite, merci de réessayer ultérieurement.', {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      })
    } else {
      // captchaRef.current.reset();
    }
  };

  return (
    <>
      <div className={'column is-full'} id={'contact'}>

        <div className={'container contactBody'}>

          <div className={'columns is-12 is-multiline'}>

            <div className={'column is-5-desktop is-12-tablet contentContact'}>

              <div className={'column is-12-desktop'}>
                <TitleComponent title={mail.Title} isHome={true}/>
              </div>

              <div className={'column is-12-desktop'}>
                <p className={'contentMail'}>{mail.Content}</p>
              </div>

            </div>

            <div className={'column is-7-desktop is-12-tablet m-auto'}>
              <form onSubmit={handleSubmit(onSubmit)} className={'columns is-multiline m-auto'}>

                <div className={'column is-5-desktop is-12-tablet m-auto'}>
                  {/* Nom */}
                  <div className="column is-12-desktop">

                    <TextInput label="Nom*" mt={'md'}
                               {...register('firstname',  { required: true })}
                               placeholder={'Nom'}
                               error={errors.firstname && <p className={'error'}>Le champ Nom est obligatoire !</p>}
                               radius={'md'}
                    />
                  </div>

                  {/* Prénom */}
                  <div className="column is-12-desktop">

                    <TextInput label="Prénom*" mt={'md'}
                               {...register('lastname', { required: true })}
                               placeholder={'Prénom'}
                               error={errors.lastname && <p className={'error'}>Le champ Prénom est obligatoire !</p>}
                               radius={'md'}
                    />
                  </div>

                  {/* Email */}
                  <div className="column is-12-desktop">

                    <TextInput label="Email*" mt={'md'}
                               {...register('email', { required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i  })}
                               placeholder={'Email'}
                               error={
                                 errors.email && errors.email.type === 'required' && <p className={'error'}>Le champ Email est obligatoire !</p> ||
                                 // eslint-disable-next-line react/no-unescaped-entities
                                 errors.email && errors.email.type === 'pattern' && <p className={'error'}>L'email renseigner n'est pas correct !</p>
                               }
                               radius={'md'}
                    />
                  </div>

                  {/* Phone */}
                  <div className="column is-12-desktop">

                    <TextInput label="Numéro de téléphone*" mt={'md'}
                               {...register('phone', { required: true, pattern: /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/i  })}
                               placeholder={'Numéro de téléphone'}
                               error={
                                 errors.phone && errors.phone.type === 'required' && <p className={'error'}>Le champ Numéro de téléphone est obligatoire !</p> ||
                                 // eslint-disable-next-line react/no-unescaped-entities
                                 errors.phone && errors.phone.type === 'pattern' && <p className={'error'}>Le numéro de téléphone renseigner n'est pas correct !</p>
                               }
                               radius={'md'}
                    />
                  </div>
                </div>

                <div className={'column is-7-desktop is-12-tablet m-auto'}>
                  {/*Services*/}
                  <div className="column is-12-desktop columns is-multiline">
                    {/*Cache un input pour la validation des services*/}
                    <TextInput label="Services*" className={'is-hidden'}
                      {...register('services', { required: true} )}
                      value={value}
                      disabled variant="unstyled"/>

                    {/*Opération de selection des services*/}
                    <CheckboxGroup
                      value={value} onChange={(value) => {
                        setValueCheck(value)
                        setValue('services', value.toString());
                        trigger('services').then();
                    }}
                      className={'columns is-multiline column is-12-desktop'}
                      label="Pour quel service souhaitez-vous des informations ?*"
                      error={errors.services && errors.services.type === 'required' && <p className={'error'}>Il est obligatoires de sélectionner au moins 1 des services proposer !</p>}                    >
                      {checkboxService}
                    </CheckboxGroup>
                  </div>

                  {/* Content */}
                  <div className="column is-12-desktop">

                    <Textarea  label="Message*" mt={'md'}
                               {...register('content', { required: true })}
                               placeholder={'Message'}
                               error={errors.content && errors.content.type === 'required' && <p className={'error'}>Le champ Message est obligatoire !</p>}
                               radius={'md'}
                               minRows={8}
                    />
                  </div>
                </div>

                <div className="is-grouped column is-12-desktop is-12-tablet is-flex is-justify-content-end p-4">
                  <ReCAPTCHA sitekey={recaptchaPublicKey} onChange={handleChange} ref={captchaRef} />
                </div>

                <div className="is-grouped column is-12-desktop is-flex is-justify-content-end p-4 columns is-multiline">
                  <div className={'column is-8-desktop is-8-tablet is-12-mobile'}>
                    <p className={'legal'}>{mail.Legal}</p>
                  </div>
                  <div className={'column is-4-desktop is-4-tablet is-12-mobile is-flex m-auto is-justify-content-center'}>
                    <Button type={'submit'} className="button is-link">Envoyer</Button>
                  </div>
                </div>

              </form>

              <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />

            </div>

          </div>

        </div>

      </div>

    </>

  )

}
