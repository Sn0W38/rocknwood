// React
import React from 'react';
// Import
import { ParallaxBanner } from 'react-scroll-parallax';
// Lib
import { GetImg } from '../lib/utils';

export default function HomeComponent(props: any) {

  return (
      <>
        <div className={'column is-full homeBody'} id={'home'}
        >
          {/*Image principal + parallax*/}
          <div className="parralaxContainer">
            <ParallaxBanner
                layers={[
                  { image: GetImg(props.background.url), speed: -20 }
                ]}
                className="parralax"
            >

              {/*logo souris*/}
              <div className={'middle'}>
                <div className={'mouse'}></div>
              </div>
              {/*calque noir*/}
              <div className={'darkMode'}></div>
            </ParallaxBanner>
            <div className={'homeContent'}>
              <div className={'title'}>
                <h1>{props.title}</h1>
                <h2>{props.subTitle}</h2>
              </div>
            </div>
          </div>
        </div>

    </>

  )

}
