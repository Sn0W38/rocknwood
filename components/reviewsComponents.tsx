// React
import React, { useState } from 'react';
// Types
import { AvisUtilisateurEntity } from '../types/review';
import TitleComponent from './shared/titleComponent';
import CardReview from './cards/cardReview';
import Carousel from 'react-multi-carousel';

export default function RatingComponent(props: any) {
// Configuration du carousel
  const config = {
    desktop: {
      breakpoint: {
        max: 3000,
        min: 1225
      },
      items: 3,
      partialVisibilityGutter: 40
    },
    tablet: {
      breakpoint: {
        max: 1200,
        min: 900
      },
      items: 2,
      partialVisibilityGutter: 20
    },
    mobile: {
      breakpoint: {
        max: 600,
        min: 0
      },
      items: 1,
      partialVisibilityGutter: 30
    }
  }
  const reviews: AvisUtilisateurEntity[] = props.reviews;

  let listReviews: any[] = [];
  let clasReviews = reviews.length <= 3 ? 'carouselReviews': '';

  if (reviews && reviews.length !== 0) {
    listReviews = reviews.map((rev: AvisUtilisateurEntity, index: number) => {
      if (rev && rev.attributes) {
        const id = rev.id || index;

        return <CardReview key={id.toString()}
                           name={rev.attributes.Name}
                           review={rev.attributes.Review}
                           score={rev.attributes.Notations}
        />

      }

    })
  }

  return (
    <>
      <div className={'column is-full ratingBody sections container'} id={'avis'}>
        <TitleComponent title={'Témoignages'} isHome={true}/>

        <div className={'column has-text-centered'}>
          <p>{'Parce qu\'il n\'y a pas plus parlant que l\'avis de clients, découvrez les retours d\'expérience authentiques sur nos services de rénovation murale, enduits imitation pierre, joints de pierre et charpente bois, reflétant notre dévouement envers un travail qualitatif au travers d\'une expérience client personnalisée.'}</p>
        </div>

        <Carousel autoPlay={reviews.length > 3}
                  shouldResetAutoplay={reviews.length > 3}
                  infinite={reviews.length > 3}
                  showDots={false}
                  customTransition="all 1s ease-in"
                  containerClass="container"
                  className={clasReviews}
                  sliderClass="columns"
                  itemClass="column is-3-desktop is-6-tablet is-12-mobile"
                  minimumTouchDrag={80}
                  renderDotsOutside={true}
                  responsive={config}
                  swipeable
        >
          {listReviews}
        </Carousel>
      </div>

    </>

  )

}
