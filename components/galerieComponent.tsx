import React, {useState} from 'react';
// Lib
import PhotoAlbum from 'react-photo-album';
import Lightbox from 'yet-another-react-lightbox';
// lightbox styles
import 'yet-another-react-lightbox/styles.css';
import 'yet-another-react-lightbox/plugins/thumbnails.css';
import "yet-another-react-lightbox/plugins/captions.css";
// lightbox plugins
import {Fullscreen, Slideshow, Thumbnails, Zoom, Captions} from 'yet-another-react-lightbox/plugins';
/// Types
import {Galerie} from '../types/galerie';

export default function GalerieComponent(props: { images: any[] }) {
    // Trigger les duplications de photos
    const removeDuplicates = (photos: Galerie[]) => {
        const unique: Galerie[] = [];
        const uniqueIds: string[] = [];
        photos.forEach((photo: Galerie) => {
            if (!uniqueIds.includes(photo.src)) {
                unique.push(photo);
                uniqueIds.push(photo.src);
            }
        });
        return unique;
    }
    const photos: Galerie[] = removeDuplicates(props.images);
    const [index, setIndex] = useState(-1);

    return (
        <>
            <div>
                <PhotoAlbum
                    layout="rows"
                    photos={photos}
                    padding={0}
                    targetRowHeight={350}
                    spacing={10}
                    defaultContainerWidth={100}
                    onClick={({index}) => setIndex(index)}
                />;

                <Lightbox
                    slides={photos}
                    open={index >= 0}
                    index={index}
                    close={() => setIndex(-1)}
                    plugins={[Fullscreen, Slideshow, Thumbnails, Zoom, Captions]}
                />

            </div>
        </>
    );

}
