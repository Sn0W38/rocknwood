// REACT BASIC
import React from 'react';
import Link from 'next/link';
// Components
import TitleComponent from './shared/titleComponent';
// Styles
import 'react-multi-carousel/lib/styles.css';
// Lib
import Carousel from 'react-multi-carousel';
import { Fade } from 'react-awesome-reveal';
import { GenerateCardRealisations, } from '../lib/utils';
// Types
import {  RealisationEntity } from '../types/realisations';

export default function RealisationsComponent(props: any) {
  // Les 10 dernieres réalisation
  const realisations: RealisationEntity[] = props.realisations;
  // Configuration du carousel
  const config = {
    desktop: {
      breakpoint: {
        max: 3000,
        min: 1225
      },
      items: 3,
      partialVisibilityGutter: 40
    },
    tablet: {
      breakpoint: {
        max: 1200,
        min: 900
      },
      items: 2,
      partialVisibilityGutter: 30
    },
    mobile: {
      breakpoint: {
        max: 600,
        min: 0
      },
      items: 1,
      partialVisibilityGutter: 30
    }
  }

  let listRealisation;

  if (realisations) {
    listRealisation = GenerateCardRealisations(realisations);
  }

  return (
    <>
      <div id={'realisations'} className={'column is-full sections'}>
        <div className={'bodyRealisations columns is-12 is-multiline'}>
          <TitleComponent title={'Réalisations'} isHome={true}/>

          <div className={'project carouselRealisations column is-12'}>

            <Carousel autoPlay={false}
                      shouldResetAutoplay={false}
                      customTransition="all 1s ease-in"
                      containerClass="container"
                      sliderClass="columns"
                      itemClass="column is-3-desktop is-6-tablet is-10-mobile"
                      minimumTouchDrag={70}
                      renderDotsOutside={true}
                      responsive={config}
                      swipeable
            >
              {listRealisation}
            </Carousel>

          </div>
          {/*@ts-ignore*/}
          <Fade className={'m-auto'}>
            <div className={'realisationPlus column is-12'}>
              <hr/>
              <Link href={'/realisations'} scroll={true}>
                <button className={'button is-outlined'}>
                  Voir plus
                </button>
              </Link>
            </div>
          </Fade>
        </div>
      </div>
    </>

  )
}
