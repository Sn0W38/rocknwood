// React
import React, {useEffect, useState} from 'react';
import Image from 'next/image';
import Link from 'next/link';
import {Link as LinkScroll} from 'react-scroll';
import {useRouter} from 'next/router';
// Lib
import {GetLogo} from '../../lib/request';
// Component
import LoaderComponent from './loaderComponent';
// Types
import {ServiceEntity} from '../../types/services';
// CookieConsent
import CookieConsent from 'react-cookie-consent';
import {getCookie, getCookies, removeCookies, setCookie} from 'cookies-next';
import {GoogleAnalytics} from 'nextjs-google-analytics';

export default function NavBarComponent() {
    const router = useRouter();
    // Récupère les informations de la barre de navigation
    const {loading, error, data} = GetLogo();
    // Hamburger
    const [isActive, setIsActive] = useState(false);
    // Dropdown
    const [isDropActive, setIsDropActive] = useState(false);
    // Couleur de la barre de navigation
    const [color, setColor] = useState(false);
    // Est-ce une page ?
    const [page, setIsPage] = useState(false);
    // L'utilisateur accepte l'usage des cookies ?
    const [consent, setIsConsent] = useState(getCookie('CookieConsent') === true);

    useEffect(() => {
        // Écoute event scroll
        const changeColor = () => {
            const scrollY: number = window?.scrollY;

            if (scrollY >= window.innerHeight && !page) {
                setColor(true);
            } else if (page) {
                setColor(true);
            } else {
                setColor(false);
            }
        };

        router.events.on('routeChangeStart', changeColor);
        window.addEventListener('scroll', changeColor);

        if (router.pathname !== '/') {
            setColor(true);
            setIsPage(true);
        } else {
            setIsPage(false);
        }

    }, [page, router.events, router.pathname]);


    // Supprime tous les cookies de l'application
    const cookieRemove = () => {
        // Supprime tous les cookies
        const cookies = Object.entries(getCookies());
        cookies.forEach((c => {
            removeCookies(c[0]);
        }));
        // L'utilisateur ne souhaite pas de cookie
        setIsConsent(false);
        // Ajoute un cookie pour valider le refus de l'utilisateur
        setCookie('CookieConsent', false);

    };


    if (loading) {
        return <LoaderComponent/>;
    } else {
        const seo = data?.referencement.data.attributes;
        const services: ServiceEntity[] = data?.services.data;
        const reviews = data?.avisUtilisateurs.data;

        let listServices: any[] = [];

        if (services && services.length) {
            listServices = services.map((srv: ServiceEntity, index: number) => {
                if (srv && srv.attributes && srv.attributes.title && srv.id) {
                    const id = srv.id || index;
                    return <span className="navbar-item" key={id.toString()}><Link
                        href={`/services/${srv.attributes.slug}`} scroll={true}>{srv.attributes.title}</Link></span>;
                }
            });
        }

        const title = seo?.title;
        const logo = seo?.icon.data.attributes.url;
        const width = seo?.icon.data.attributes.width;
        const height = seo?.icon.data.attributes.height;

        return (
            <>
                {/*Gestion des cookies*/}
                <CookieConsent
                    style={{backgroundColor: 'rgba(242,242,242,1)', color: 'black', fontSize: '12px'}}
                    enableDeclineButton
                    onDecline={() => {
                        cookieRemove();
                    }}
                    declineButtonText={'Refuser'}
                    declineButtonStyle={{backgroundColor: '#B43225', borderRadius: '.5em', color: 'white'}}
                    onAccept={() => {
                        setIsConsent(true);
                    }}
                    buttonText={'D\'accord'}
                    buttonStyle={{backgroundColor: '#34A648', borderRadius: '.5em', color: 'white'}}
                >
                    <p>
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        Nous utilisons des cookies et des technologies similaires afin d'assurer le bon fonctionnement
                        de notre site et ainsi analyser son trafic.
                        Cela nous permet de continuer à améliorer et personnaliser votre expérience de navigation.
                        En acceptant ce message, vous consentez à l’utilisation des cookies pour l’ensemble de ces
                        finalités, conformément à notre <a href={'/legal'}>Politique de confidentialité</a>.
                    </p>
                </CookieConsent>
                {consent && <GoogleAnalytics strategy={'lazyOnload'}
                                             gaMeasurementId={process.env.NEXT_PUBLIC_GA_MEASUREMENT_ID}/>}

                <nav className={color ? 'navbar is-fixed-top header-dark' : 'navbar is-fixed-top header-bg'}
                     role="navigation" aria-label="main navigation">
                    <div className="navbar-brand">
                        {/*Icon*/}
                        {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                        <a className={'navbar-item'} href="/">
                            <span className={'logo'}>
                                <Image src={logo} alt={title} width={width} height={height} priority/>
                            </span>
                        </a>

                        {/*Hamburger*/}
                        <a
                            onClick={() => {setIsActive(!isActive)}}
                            role="button"
                            className={`navbar-burger burger ${isActive ? 'is-active' : ''}`}
                            aria-label="menu"
                            aria-expanded="false"
                            data-target="navbarHome"
                        >
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    {/*Navbar droite*/}
                    <div id="navbarHome" className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
                        <div className="navbar-end">
                            <div className="navbar-item">
                                {/*Accueil*/}
                                <span className="navbar-item">
                                    {!page && (
                                        <LinkScroll activeClass="active" smooth spy to={'home'}
                                                    onClick={() => setIsActive(!isActive)}>Accueil</LinkScroll>
                                    )}
                                    {page && (
                                        <a href={'/'}>Accueil</a>
                                    )}
                                </span>

                                {/*Présentations pro + entreprise*/}
                                <span className="navbar-item">
                                    {!page && (
                                        <LinkScroll activeClass="active" smooth spy to={'pres'}
                                                    onClick={() => setIsActive(!isActive)}>Présentation</LinkScroll>
                                    )}
                                    {page && (
                                        <a href={'/#pres'}>Présentation</a>
                                    )}
                                </span>

                                {/*Services*/}
                                <span className="navbar-item">
                                    {!page && (
                                        <LinkScroll activeClass="active" smooth spy to={'services'}
                                                    onClick={() => setIsActive(!isActive)}>Services</LinkScroll>
                                    )}
                                    {page && (
                                        <a href={'/#services'}>Services</a>
                                    )}
                                </span>

                                {/*Réalisations*/}
                                <div className={`navbar-item has-dropdown is-hoverable ${isDropActive ? 'is-active' : ''}`}>
                                    <span className="navbar-link">
                                      {!page && (
                                          <LinkScroll activeClass="active" smooth spy to={'realisations'}
                                                      onClick={() => setIsActive(!isActive)}>Réalisations</LinkScroll>
                                      )}
                                        {page && (
                                            <a href={'/#realisations'}>Réalisations</a>
                                        )}
                                    </span>
                                    <div className="navbar-dropdown is-boxed">
                                        {listServices}
                                    </div>
                                </div>


                                <span className="navbar-item">
                                    <Link href={'/galerie'}>Galerie</Link>
                                </span>

                                {/*Avis*/}
                                {reviews && reviews.length !== 0 && (
                                    <span className="navbar-item">
                                    {!page && (
                                        <LinkScroll activeClass="active" smooth spy to={'avis'}
                                                    onClick={() => setIsActive(!isActive)}>Avis</LinkScroll>
                                    )}
                                    {page && (
                                        <a href={'/#avis'}>Avis</a>
                                    )}
                                    </span>
                                )}

                                {/*Contact*/}
                                <span className="navbar-item">
                                    {!page && (
                                        <LinkScroll activeClass="active" smooth spy to={'contact'} onClick={() => setIsActive(!isActive)}>
                                            <button className={'button is-link'}>Contact</button>
                                        </LinkScroll>
                                    )}
                                    {page && (
                                        <a href={'/#contact'}>
                                            <button className={'button is-link'}>Contact</button>
                                        </a>
                                    )}
                                </span>
                            </div>
                        </div>
                    </div>
                </nav>
            </>

        );
    }
}
