// React
import React, { useCallback, useEffect, useState } from 'react';
// Lib
import InfiniteScroll from 'react-infinite-scroll-component';
import { GetRealisationsByPage } from '../../lib/request';
// Types
import { RealisationEntity } from '../../types/realisations';
// Component
import CardRealisationComponent from '../cards/cardRealisation';

export default function BlockRealisationComponent(props: any) {
  const isHome = props.isHome ? props.isHome : false;

  const categories = props.categorie;
  // Liste des réalisations
  let [realisation, setRealisations] = useState([]);
  // Nombre de pages maximal
  const pageTotal = parseInt(props.realisations?.meta?.pagination?.pageCount);
  // Page actuel
  const [currentPage, setCurrentPage] = useState(2)
  // Charger au scroll ?
  const [hasMore, setHasMore] = useState(pageTotal !== 1);
  const [messageEnd, setMessageEnd] = useState(<div></div>);

  // Initialise les données pour le traitement
  useEffect(() => {
    setRealisations(props.realisations.data);
    setCurrentPage(parseInt(props.realisations?.meta?.pagination?.page) + 1);
  }, [props.realisations]);

  const fetchItems = useCallback(
    async () => {
      if (pageTotal !== 1) {
        // eslint-disable-next-line react/no-unescaped-entities
        setMessageEnd(<div className={'endMessage'}><p>Il n'y plus d'autres réalisations pour le moment</p></div>);
        setCurrentPage(currentPage + 1)

        if (currentPage !== 0 && currentPage <= pageTotal) {
          const {loading, data} = await GetRealisationsByPage(currentPage, categories);

          if (currentPage === pageTotal) {
            setHasMore(false)
          }

          if (!loading && realisation.length !==0) {
            // @ts-ignore
            setRealisations([...realisation, ...data.realisations.data]);
          }
        }
      } else {
        setHasMore(false)
      }

    },
    [categories, currentPage, pageTotal, realisation]
  );

  let listRealisation;

  if (realisation && realisation.length !== 0) {
    listRealisation = realisation.map((projet: RealisationEntity, index: number) => {
      if (projet) {
        let cl = '';
        if (realisation && realisation.length === 1) {
          cl = 'column is-12-desktop is-12-tablet is-12-mobile '
        } else if (realisation.length === 2) {
          cl = 'column is-6-desktop is-6-tablet is-12-mobile '
        }
        else {
          cl = 'column is-4-desktop is-6-tablet is-12-mobile ';
        }
        return <div key={index} className={cl}>
          <CardRealisationComponent realisations={projet} isHome={isHome} />
        </div>
      }
    })

    return (

      <>
        <InfiniteScroll next={fetchItems}
                        hasMore={hasMore}
                        loader={<h4>Chargement...</h4>}
                        dataLength={realisation.length}
                        endMessage={messageEnd}
                        className={'cardSize'}
        >

          <div className={'columns is-multiline column is-full m-auto'}>
            {listRealisation}
          </div>

        </InfiniteScroll>
      </>

    )

  } else {
    return (
      // eslint-disable-next-line react/no-unescaped-entities
      <><p>Il n'y à pas de réalisations pour le moment</p></>
    )
  }

}
