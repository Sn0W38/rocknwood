// react
import React from 'react';
import { Fade } from 'react-awesome-reveal';
// Lib

export default function TitleComponent(props: any) {

  const title = props.title;
  const isHome = props.isHome ? props.isHome : false;
  return (
    <>
      <div className={'column is-full titleComponent'}>
        {/*@ts-ignore*/}
        <Fade direction={'down'}>
            {isHome ? (<h2 className={'title'} >{title}</h2>) : (<h1 className={'title'} >{title}</h1>)}
        </Fade>
      </div>

    </>

  )

}
