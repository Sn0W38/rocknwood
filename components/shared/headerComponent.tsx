// react
import Head from 'next/head';
import React from 'react';
// Lib
import { GetImg } from '../../lib/utils';
// Component

export default function HeaderComponent(props: any) {
  const title = props.title;
  const titleUrl = props.titleUrl || '';
  const description = props.description;
  const keywords = props.keywords || '';
  const img = props.img;
  const icon = props.icon || '/uploads/small_bf12321a0c.png';

  let titleMeta = '';

  if (title && titleUrl) {
    titleMeta = title + ' | ' + titleUrl;
  } else {
    titleMeta = title;
  }

  return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta charSet="utf-8"/>
          <meta itemProp="description" content={description}/>
          <meta itemProp="keywords" content={keywords}/>
          <title>{titleMeta}</title>

          <meta itemProp="name" content={titleMeta}/>
          <meta itemProp="image" content={`${GetImg(img)}`}/>

          <meta property="og:title" content={titleMeta} key="ogtitle"/>
          <meta property="og:description" content={description} key="ogdesc"/>
          <meta property="og:site_name" content={'Rock\'n Wood'} key="ogsitename"/>
          <meta property="og:url" content={'rockn-wood.fr'}/>
          <meta property="og:image" content={`${GetImg(img)}`}/>

          <meta name="twitter:card" content="summary"/>
          <meta name="twitter:site" content="rock'n wood"/>
          <meta name="twitter:title" content={titleMeta}/>
          <meta name="twitter:description" content={description}/>
          <meta name="twitter:image" content={`${GetImg(img)}`}/>

          <link rel="icon" href={`${GetImg(icon)}`} type="image/x-icon"/>
          <link rel="icon" href={icon} type="image/x-icon"/>
        </Head>
      </>

  );

}