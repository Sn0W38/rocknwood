// react
import React from 'react';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween } from 'react-gsap';

export default function VeticalLineComponent(props: any) {

  const trigger = props.trigger;

  return (

    <div className={'column is-full verticalLineContent'}>
      <Controller>
        <Scene duration={800} triggerElement={`#${trigger}`}>
          {(progress: any) => {
            return (
              <Tween
                to={{
                  transform: "scaleY(1)"
                }}
                ease="Strong.easeOut"
                totalProgress={progress}
                paused
              >
                <div id={trigger} className="vertical-line"></div>
              </Tween>
            );
          }}
        </Scene>
      </Controller>

    </div>

  )

}
