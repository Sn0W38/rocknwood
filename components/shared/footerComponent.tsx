// React
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
// Lib
import { FaFacebook, FaInstagram, FaPhoneAlt, FaEnvelope, FaRegCopyright, FaMapMarkerAlt } from "react-icons/fa";
import { useRouter } from 'next/router';
import { Link as LinkScroll } from 'react-scroll/modules';
import { ServiceEntity } from '../../types/services';


export default function FooterComponent(props: any) {
  const router = useRouter()
  // Est-ce une page ?
  const [page, setIsPage] = useState(false);

  useEffect(() => {
    if (router.pathname !== '/') {
      setIsPage(true);
    } else {
      setIsPage(false);
    }
  }, [router.pathname]);

  const title: string = props.title.toString();
  const phone: string = props.phone.toString();
  const adresse: string = props.adresse.toString();
  const email: string = props.email.toString();
  const facebook: string = props.facebook.toString();
  const instagram: string = props.instagram.toString();
  const services = props.services;
  const review = props.reviews;

  const listService = services.map((serv: ServiceEntity, index: any) => {
    if (serv && serv.id && serv.attributes.title && serv.attributes.slug) {
      const id = serv.id.toString() || index;
      return <li key={id}>
        <Link href={`/services/${serv.attributes.slug}`} scroll={true}>{serv.attributes.title}</Link>
      </li>
    }
  })

  return (
    <>

      <footer className={'footer footerContent column is-full'}>
        <div className={'container'}>

          <div className={'columns is-multiline'}>

            <div className={'footerInfoFirst column is-full columns is-multiline'}>
              {/* Section Adresse + Email + Phone */}
              <div className={'column is-4-desktop is-6-tablet is-10-mobile columns is-multiline contentFirst'}>
                <div className={'column is-2-desktop is-flex'}>
                  <div className={'m-auto colorMain'}>
                    <FaMapMarkerAlt size={40} />
                  </div>
                </div>
                <div className={'column is-8-desktop'}>
                  <h4 className={'title colorMain'}>Adresse</h4>
                  <p>{adresse}</p>
                </div>
              </div>
              <div className={'column is-4-desktop is-6-tablet is-10-mobile columns is-multiline contentFirst'}>
                <div className={'column is-2-desktop is-flex'}>
                  <div className={'m-auto colorMain'}>
                    <FaPhoneAlt size={40} />
                  </div>
                </div>
                <div className={'column is-8-desktop'}>
                  <h4 className={'title colorMain'}>Téléphone</h4>
                  <p>{phone}</p>
                </div>
              </div>
              <div className={'column is-4-desktop is-6-tablet is-10-mobile columns is-multiline contentFirst'}>
                <div className={'column is-2-desktop is-flex'}>
                  <div className={'m-auto colorMain'}>
                    <FaEnvelope size={40} />
                  </div>
                </div>
                <div className={'column is-8-desktop'}>
                  <h4 className={'title colorMain'}>Email</h4>
                  <p>{email}</p>
                </div>
              </div>

            </div>


            {/* Section Navbar + Service + Social network */}
            <div className={'column is-12-desktop columns is-multiline'}>
              <div className={'column is-full columns is-multiline footerInfoSecond'}>

                <div className={'column is-4-desktop is-6-tablet is-12-mobile is-flex'}>
                  <div className={'ml-auto mr-auto'}>
                    <h4 className={'title colorMain'}>Services</h4>
                    <div className={'content'}>
                      <ul>
                        {listService}
                      </ul>
                    </div>
                  </div>
                </div>

                <div className={'column is-4-desktop is-6-tablet is-12-mobile is-flex'}>
                  <div className={'ml-auto mr-auto'}>
                    <h4 className={'title colorMain'}>Pages</h4>
                    <div className={'content'}>
                      <ul>

                        {/*Accueil*/}
                        <li>
                          {!page && (
                            <LinkScroll activeClass="active" smooth spy to={'home'}>Accueil</LinkScroll>
                          )}
                          {page && (
                            <Link href={'/'} scroll={true}>Accueil</Link>
                          )}
                        </li>

                        {/*Présentations pro + entreprise*/}
                        <li>
                          {!page && (
                            <LinkScroll activeClass="active" smooth spy to={'pres'}>Présentation</LinkScroll>
                          )}
                          {page && (
                            <Link href={'/#pres'} scroll={true}>Présentation</Link>
                          )}
                        </li>

                        {/*Services*/}
                        <li>
                          {!page && (
                            <LinkScroll activeClass="active" smooth spy to={'services'}>Services</LinkScroll>
                          )}
                          {page && (
                            <Link href={'/#services'} scroll={true}>Services</Link>
                          )}
                        </li>

                        {/*Réalisations*/}
                        <li>
                          {!page && (
                            <LinkScroll activeClass="active" smooth spy to={'realisations'}>Réalisations</LinkScroll>
                          )}
                          {page && (
                            <Link href={'/#realisations'} scroll={true}>Réalisations</Link>
                          )}
                        </li>

                        {/*Avis*/}
                        {review && review.length !== 0 && (
                          <li>
                            {!page && (
                              <LinkScroll activeClass="active" smooth spy to={'avis'}>Avis</LinkScroll>
                            )}
                            {page && (
                              <Link href={'/#avis'} scroll={true}>Avis</Link>
                            )}
                          </li>
                        )}

                        {/*Contact*/}
                        <li>
                          {!page && (
                            <LinkScroll activeClass="active" smooth spy to={'contact'}>Contact</LinkScroll>
                          )}
                          {page && (
                            <Link href={'/#contact'} scroll={true}>Contact</Link>
                          )}
                        </li>
                        <li>
                          <Link href={'/legal'} scroll={true}>Mentions Légale</Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div className={'column is-4-desktop is-6-tablet is-12-mobile is-flex'}>
                  <div className={'ml-auto mr-auto'}>
                    {(facebook || instagram) && (
                      <div>
                        <h4 className={'title colorMain'}>Réseaux sociaux</h4>
                        <div className={'socialContent'}>
                          {facebook && (
                            <a target='_blank' href={facebook} rel={'noreferrer'}>
                              <FaFacebook size={'40'} className={'fb'}/>
                            </a>
                          )}
                          {instagram && (
                            <a target='_blank' href={instagram} rel='noreferrer'>
                              <FaInstagram size={'40'} className={'inst'}/>
                            </a>
                          )}
                        </div>
                      </div>
                    )}

                  </div>
                </div>

              </div>
            </div>

            {/* Section Copyright + Mention Legal */}
            <div className={'column is-full copyright'}>
              <FaRegCopyright /> {title} - All Rights Reserved
            </div>
          </div>

        </div>

      </footer>
    </>

  )

}
