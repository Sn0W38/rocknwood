// React
import React from 'react';

export default function LoaderComponent() {

  return (
    <>
      <div className="loader-wrapper is-active">
        <div className="loader"></div>
      </div>
    </>
  )
}
