// React
import React  from 'react';
import dynamic from 'next/dynamic';

export default function MapComponent(props: any) {

  const MapWithNoSSR = dynamic(() => import('./map'), {
    ssr: false,
  });

  return (
    <>
      <div className={'column is-full mapBody'} id={'map'}>
        <div className={'container'}>
          {/*Leaflet Map*/}
          <MapWithNoSSR />
        </div>
      </div>
    </>
  )

}
