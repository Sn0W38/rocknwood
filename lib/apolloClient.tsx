import { ApolloClient, ApolloLink, createHttpLink, InMemoryCache } from '@apollo/client';

// URL DE L'API
const link: ApolloLink = createHttpLink({
  uri: process.env.API,
  headers: {
    "Authorization": `Bearer ${process.env.TOKEN}`
  },
  fetch: fetch
});

/**
 * cache-first : c'est la valeur par défaut, le client recherche le résultat dans le cache avant de faire une requête.
 * cache-and-network : le client retournera le contenu du cache, mais fera tout de même la requête afin de le mettre à jour, permet d'avoir une réponse rapide.
 * network-only : le client ne retournera jamais le contenu du cache pour cette requête et fera systématiquement un appel réseau.
 * cache-only : le client ne fera aucun appel réseau et se contentera de lire le cache.
 * no-cache : le client fera un appel réseau et ne lira pas le cache, mais au contraire de network-only, le résultat de la requête ne sera pas écrit dans le cache.
 */
const client = new ApolloClient({
  ssrMode: typeof window === 'undefined',
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache',
    }
  }
});

export default client;
