// REACT BASIC
import {DocumentNode, gql, useQuery} from '@apollo/client';
// LIB
import apolloClient from './apolloClient';
// Types
import {ServiceEntityResponse} from '../types/services';
import {ApolloClientResult} from '../types/strapi';
import {RealisationRelationResponseCollection} from '../types/realisations';
import {formEmailData} from '../types/form';

// Data des réalisations
const REALISATION_DATA = `
data {
  id,
  attributes {
    seotitle,
    seodescription,
    seokeywords,
    title,
    slug,
    content,
    categorie {
      data {
        id,
        attributes {
          title
        }
      }
    }
    after {
      data {
        attributes {
                url,
                width,
                height
        }
      }
    },
    before {
      data {
        attributes {
                url,
                width,
                height
        }
      }
    },
    top,
    galerie {
        data {
            attributes {
                url,
                width,
                height
            }
        }
    },
    createdAt,
    updatedAt,
    publishedAt
  }
}
`;

// =================================================
// Get init data
// SEO du site
const referencement: String = `
referencement {
  data {
    id,
    attributes {
      title,
      sub_title,
      title_url,
      phone,
      email,
      adresse,
      facebook,
      instagram,
      icon {
        data {
          attributes {
            url
          }
        }
      },
      logo {
        data {
          attributes {
            url,
            width,
            height
          }
        }
      },
      description,
      background {
        data {
          attributes {
            url
          }
        }
      },
    }
  }
}`;
// Présentation personnelle
const presentationPro: String = `
presentationPro {
  data {
    attributes {
      title,
      content,
      img {
        data {
          attributes {
            url
          }
        }
      }
    }
  }
}`;
// Présentation entreprise
const presentationEnterprise: String = `
presentationEntreprise {
  data {
    attributes {
      title,
      content,
      img {
        data {
          attributes {
            url
          }
        }
      }
    }
  }
}
`;
// Présentation services
const services: String = `
services(pagination: {limit: 4}) {
  data {
    id,
    attributes {
      title,
      slug,
      description,
      description_short,
      icon {
        data {
          attributes {
            url
          }
        }
      }
    }
  }
}
`;
// Réalisations (max 10), Trier par date de mise à jour
const realisation_limit10: String = `
realisations(pagination: {limit: 10}, sort: "updatedAt:desc") {
  ${REALISATION_DATA}
}
`;
// Réalisation (infini)
const realisation_limit25: String = `
realisations(pagination: {limit: 25}, sort: "updatedAt:desc") {
  ${REALISATION_DATA}
}
`;
// Liste des catégories
const categories: String = `
categories {
  data {
    id,
    attributes {
      title,
    }
  }
}
`;
// Get Mail Info
const mail_info: String = `
mail {
  data {
    attributes {
      Title,
      Content,
      Legal,      
    }
  }
}
`;
// Get Mail Info
const reviews: String = `
avisUtilisateurs {
  data {
    id,
    attributes {
      Name,
      Notations,
      Review,
      createdAt
    }
  }
}
`;
// Init les datas de la 1er page
const GET_INIT: DocumentNode = gql`
    query init {
        ${referencement},
        ${presentationPro},
        ${presentationEnterprise},
        ${services},
        ${realisation_limit10},
        ${categories},
        ${mail_info},
        ${reviews}
    }
`;

// Initialisation de la 1er page
export function GetInit() {
    return apolloClient.query({
        query: GET_INIT,
    });
}

// =================================================
// Get Logo base
const GET_LOGO: DocumentNode = gql`
    query referencement {
        referencement {
            data {
                attributes {
                    title,
                    sub_title,
                    icon {
                        data {
                            attributes {
                                url,
                                width,
                                height
                            }
                        }
                    },
                    logo {
                        data {
                            attributes {
                                url,
                                width,
                                height
                            }
                        }
                    },
                }
            }
        },
        ${services},
        ${reviews}
    }
`;

export function GetLogo() {
    return useQuery(GET_LOGO);
}

// =================================================
// GetServices
const GET_ALL_SERVICES: DocumentNode = gql`
    query getServices {
        ${services}
    }
`;

export function GetServices(): Promise<ApolloClientResult<ServiceEntityResponse>> {
    return apolloClient.query({
        query: GET_ALL_SERVICES,
    });
}

// =================================================
// GetService by title
const GET_SERVICE: DocumentNode = gql`
    query GetServiceBySlug($filters: ServiceFiltersInput) {
        services(filters: $filters) {
            data {
                id,
                attributes {
                    title,
                    slug,
                    description,
                    description_short,
                    icon {
                        data {
                            attributes {
                                url
                            }
                        }
                    }
                }
            }
        }
    }
`;

export function GetService(slug: String): Promise<ApolloClientResult<ServiceEntityResponse>> {
    return apolloClient.query({
        query: GET_SERVICE,
        variables: {
            filters: {
                slug: {
                    eq: slug,
                },
            },
        },
    });
}

// =================================================
// GetRealisation max 10
const GET_REALISATION_LIMIT10: DocumentNode = gql`
    query realisationLimit {
        ${realisation_limit10},
    }
`;

const GET_REALISATION_LIMIT25: DocumentNode = gql`
    query realisations {
        ${realisation_limit25},
    }
`;

export function GetRealisations(): Promise<ApolloClientResult<RealisationRelationResponseCollection>> {
    return apolloClient.query({
        query: GET_REALISATION_LIMIT10,
    });
}

export function GetAllRealisations(): Promise<ApolloClientResult<RealisationRelationResponseCollection>> {
    return apolloClient.query({
        query: GET_REALISATION_LIMIT25,
    });
}

// =================================================
// GetRealisation by slug
const GET_REALISATION_BY_SLUG: DocumentNode = gql`
    query realisationBySlug($filters: RealisationFiltersInput) {
        realisations(filters: $filters) {
            ${REALISATION_DATA}
        },
    }
`;

export function GetRealisationsBySlug(slug: String): Promise<ApolloClientResult<RealisationRelationResponseCollection>> {
    return apolloClient.query({
        query: GET_REALISATION_BY_SLUG,
        variables: {
            filters: {
                slug: {
                    eq: slug,
                },
            },
        },
    });
}

// =================================================
// GetAllRealisation by page
const GET_REALISATION_BY_PAGE: DocumentNode = gql`
    query realisationByPage($pagination: PaginationArg!, $filters: RealisationFiltersInput) {
        realisations(pagination: $pagination, filters: $filters, sort:"updatedAt:desc") {
            ${REALISATION_DATA},
            meta {
                pagination {
                    total,
                    page,
                    pageCount
                }
            }
        },
    }
`;

export function GetRealisationsByPage(page: number, categorie?: string): Promise<ApolloClientResult<RealisationRelationResponseCollection>> {
    let filterCat;
    if (categorie !== '*') {
        filterCat = {
            categorie: {
                title: {
                    eq: categorie,
                },
            },
        };
    }

    return apolloClient.query({
        query: GET_REALISATION_BY_PAGE,
        variables: {
            pagination: {
                pageSize: 6,
                page: page,
            },
            filters: filterCat,
        },
    });
}

// ====================================================================


// ====================================================================
// ====================================================================
// ====================================================================

/**
 * Service d'envoi de mail à partir de Strapi
 */
export function sendEmail(form: formEmailData) {
    const base = process.env.UPLOADS;
    const route = '/api/email-form/';
    const method = 'POST';

    return fetch(base + route, {
        method,
        credentials: 'same-origin',
        body: JSON.stringify(form),
        headers: {
            'Authorization': `Bearer ${process.env.TOKEN}`,
        },
    });
}

/**
 * Service d'envoi de mail à partir de Strapi
 */
export function captcha(token: string): Promise<boolean> {
    const base = process.env.UPLOADS;
    const route = '/api/recaptcha';
    const method = 'POST';

    return new Promise((resolve, reject) => {
        if (token) {
            fetch(base + route, {
                method,
                credentials: 'same-origin',
                body: JSON.stringify({token}),
                headers: {
                    'Authorization': `Bearer ${process.env.TOKEN}`,
                },
            })
                .then((response) => response.json())
                .then((google_response) => {
                    if (google_response.success == true) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
                .catch((err) => {
                    console.log(err);
                    resolve(false);
                });
        } else {
            resolve(false);
        }

    });
}