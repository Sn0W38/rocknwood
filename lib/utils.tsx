// React
import React from 'react';
// Types
import { RealisationEntity } from '../types/realisations';
// Components
import CardRealisationComponent from '../components/cards/cardRealisation';
// ================================================================
/**
 * Retourne le nom de domain principal de l'API
 * @returns {string}
 */
export function getEnvUrl(): String {
  return process.env.FRONT ? process.env.FRONT : '';
}
// ================================================================
/**
 * Retourne le lien source de l'image
 * @param src: string
 * @return url de l'image
 */
export function GetImg(src: String): string {
  return process.env.UPLOADS ? process.env.UPLOADS + src.toString() : '';
}
// ================================================================
export function GenerateCardRealisations(realisations: RealisationEntity[]) {
  let listRealisation: any;

  if (realisations && realisations.length !== 0) {
    listRealisation = realisations.map((projet: RealisationEntity, index: number) => {
      const id = projet.id || index;
      return <CardRealisationComponent key={id.toString()} realisations={projet} isHome={true} />
    })

    return listRealisation;

  } else {
    return (
      <>
      </>
    )
  }
}
