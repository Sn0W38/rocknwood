import {ResponseCollectionMeta, UploadFileEntity, UploadFileEntityResponse} from './strapi';
import {CategorieEntityResponse} from './categories';

export interface RealisationEntityResponseCollection {
    data: RealisationEntity[];
    meta: ResponseCollectionMeta;
}

export interface RealisationEntityResponse {
    data: RealisationEntity;
}

export interface RealisationRelationResponseCollection {
    data: RealisationEntity[];
    meta?: any;
}

export interface RealisationEntity {
    id: number;
    attributes: Realisation;
}

export interface Realisation {
    title: string;
    content: string;
    slug: string;
    before?: UploadFileEntityResponse;
    after: UploadFileEntityResponse;
    top?: boolean;
    categorie: CategorieEntityResponse;
    createdAt: Date;
    updatedAt: Date;
    publishedAt: Date;
    galerie: { data: UploadFileEntity[] }
    seotitle: string;
    seodescription: string;
    seokeywords: string;
}