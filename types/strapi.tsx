export type ApolloClientResult<T> = any;

export interface Pagination {
  total: number;
  page: number;
  pageSize: number;
  pageCount: number;
}

export interface ResponseCollectionMeta {
  pagination: Pagination;
}

export interface UploadFileEntityResponse {
  data: UploadFileEntity;
}

export interface UploadFileEntity {
  id: number;
  attributes: UploadFile;
}

export interface UploadFile {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: any;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: any;
  related: any[];
  createdAt: Date;
  updatedAt: Date;
}
