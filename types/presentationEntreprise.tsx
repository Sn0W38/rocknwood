import { UploadFileEntityResponse } from './strapi';

export interface PresentationEntrepriseEntityResponse {
  data: PresentationEntrepriseEntity;
}

export interface PresentationEntrepriseEntity {
  id: number;
  attributes: PresentationEntreprise;
}

export interface PresentationEntreprise {
  title: string;
  content: string;
  img: UploadFileEntityResponse;
  createdAt?: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
