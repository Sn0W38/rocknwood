import { UploadFileEntityResponse } from './strapi';

export interface PresentationProEntityResponse {
  data: PresentationProEntity;
}

export interface PresentationProEntity {
  id: number;
  attributes: PresentationPro;
}

export interface PresentationPro {
  title: string;
  content: string;
  img: UploadFileEntityResponse;
  createdAt?: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
