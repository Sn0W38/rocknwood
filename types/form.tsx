export interface formEmailData {
  lastname: string;
  firstname: string;
  email: string;
  phone: string;
  services: string;
  content: string;
}
