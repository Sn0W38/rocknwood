export interface AvisUtilisateurEntityResponse {
  data: AvisUtilisateurEntity;
}

export interface AvisUtilisateurEntity {
  id: number;
  attributes: AvisUtilisateur;
}

export interface AvisUtilisateur {
  Name: string;
  Review: string;
  Notations: number;
  createdAt: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
