import { ResponseCollectionMeta, UploadFileEntityResponse } from './strapi';

export interface ServiceEntityResponseCollection {
  data: ServiceEntity[];
  meta: ResponseCollectionMeta;
}

export interface ServiceEntityResponse {
  data: ServiceEntity[];
}

export interface ServiceEntity {
  id: number;
  attributes: Service;
}

export interface Service {
  title: string;
  description_short: string;
  description: string;
  icon: UploadFileEntityResponse;
  slug: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
}
