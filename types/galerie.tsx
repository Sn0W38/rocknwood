export interface Galerie {
    src: string;
    width: number;
    height: number;
    alt?: string;
    description?: string;
    link?: string;
}
