import { UploadFileEntityResponse } from './strapi';

export interface ReferencementEntityResponse {
  data: ReferencementEntity;
}

export interface ReferencementEntity {
  id?: number;
  attributes: Referencement;
}

export interface Referencement {
  title: string;
  icon: UploadFileEntityResponse;
  description: string;
  background: UploadFileEntityResponse;
  sub_title: string;
  title_url: string;
  logo: UploadFileEntityResponse;
  phone: string;
  email: string;
  adresse: string;
  facebook: string;
  instagram: string;
  createdAt?: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
