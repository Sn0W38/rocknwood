import { ResponseCollectionMeta } from './strapi';

export interface CategorieEntityResponseCollection  {
  data: CategorieEntity[];
  meta: ResponseCollectionMeta;
}

export interface CategorieEntityResponse {
  data: CategorieEntity;
}

export interface CategorieEntity {
  id?: number;
  attributes: Categorie;
}

export interface Categorie {
  title?: string;
  createdAt?: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
