export interface MailEntityResponse {
  data: MailEntity;
}

export interface MailEntity {
  id: number;
  attributes: Mail;
}

export interface Mail {
  Title: string;
  Content: string;
  Legal: string;
  createdAt?: Date;
  updatedAt?: Date;
  publishedAt?: Date;
}
